package com.mozhu.springcloud.filter;

import com.mozhu.springcloud.constants.RedisKeyConstants;
import com.mozhu.springcloud.exception.BusinessException;
import com.mozhu.springcloud.util.RedisStringUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class PreFilter extends ZuulFilter {

    @Autowired
    private RedisStringUtils redisStringUtils;

    @Override
    public String filterType() {
        /**
         * 过滤器类型：
         * FilterConstants.PRE_TYPE     执行方法前执行
         * FilterConstants.ROUTE_TYPE   执行代理方法
         * FilterConstants.POST_TYPE    返回数据前执行
         * FilterConstants.ERROR_TYPE   pre和route出问题时执行，执行后再执行post
         */

        return FilterConstants.PRE_TYPE;
    }

    /**
     * 过滤器优先级，越小优先级越高
     *
     * @return
     */
    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * 是否开启过滤器
     *
     * @return
     */
    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();//获取request
        String uri = request.getRequestURI();
        /**
         * 非登录请求时开户token验证
         */
        if (uri.indexOf("/login") < 0) {
            return false;
        }
        return false;
    }

    /**
     * 过滤器执行内容
     *
     * @return
     * @throws ZuulException
     */
    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();//获取request
        String token = request.getHeader("token");
        if (StringUtils.isBlank(token)) {
            throw new BusinessException("请先登录");
        }
        String value = redisStringUtils.getKey(RedisKeyConstants.LOGIN_USER_PREV + token);
        if (StringUtils.isBlank(value)){
            throw new BusinessException("登录过期");
        }
//        ctx.set("key", "value");//传值
        return null;
    }
}
