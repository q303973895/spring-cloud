package com.mozhu.springcloud.client;

import org.springframework.stereotype.Component;

@Component
public class UserFeignClientFail implements UserFeignClient {
    @Override
    public boolean login(String name, String password) {
        return false;
    }

    @Override
    public String testRibbon(int count) {
        return null;
    }
}
