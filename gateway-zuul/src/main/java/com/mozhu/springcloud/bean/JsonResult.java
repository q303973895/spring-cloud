package com.mozhu.springcloud.bean;


public class JsonResult<T> {
    private String code;
    private String msg;
    private T data;

    public static final String SUCCESS = "200";
    public static final String ERROR = "-1";
    public static final String WARNING = "0";

    public static <T> JsonResult ok(T data) {
        return ok(data, "");
    }

    public static JsonResult ok(String msg) {
        return ok(null, msg);
    }

    public static <T> JsonResult ok(T data, String msg) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setMsg(msg);
        jsonResult.setCode(SUCCESS);
        jsonResult.setData(data);
        return jsonResult;
    }

    public static JsonResult error(String msg) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setMsg(msg);
        jsonResult.setCode(ERROR);
        jsonResult.setData(null);
        return jsonResult;
    }

    public static JsonResult warning(String msg) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setMsg(msg);
        jsonResult.setCode(WARNING);
        jsonResult.setData(null);
        return jsonResult;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
