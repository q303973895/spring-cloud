package com.mozhu.springcloud.controller;

import com.mozhu.springcloud.client.UserFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserFeignClient userFeignClient;

    @GetMapping("/login")
    public String login(String name, String password) {
        if (userFeignClient.login(name, password)) {
            return "登录成功";
        } else {
            return "登录失败";
        }
    }

    @GetMapping("/testRibbon")
    public String testRibbon() {
        for (int i = 0; i < 10; i++) {
            userFeignClient.testRibbon(i);
        }
        return "success";
    }
}
