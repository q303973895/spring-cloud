package com.mozhu.springcloud.exception;

import java.util.UUID;

/**
 * 全局异常处理
 */
public class GlobalException extends RuntimeException {
    private String code;
    private String exceptionId;//异常ID便于找出异常
    private String message;//异常信息

    public GlobalException() {
        super();
        this.exceptionId = UUID.randomUUID().toString();
    }

    public GlobalException(String message) {
        super(message);
        this.exceptionId = UUID.randomUUID().toString();
    }

    public GlobalException(String message, Throwable cause) {
        super(message, cause);
        this.exceptionId = UUID.randomUUID().toString();
    }

    public GlobalException(Throwable cause) {
        super(cause);
        this.exceptionId = UUID.randomUUID().toString();
    }

    protected GlobalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.exceptionId = UUID.randomUUID().toString();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getExceptionId() {
        return exceptionId;
    }

    public void setExceptionId(String exceptionId) {
        this.exceptionId = exceptionId;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
