package com.mozhu.springcloud.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserService {
    Logger logger = LoggerFactory.getLogger(UserService.class);

    @GetMapping("/login")
    public boolean login(String name, String password) {
        System.out.println(111);
        return "feign".equals(name) && "123457".equals(password);
    }


    @GetMapping("/testRibbon")
    public String testRibbon(int count) {
        logger.info("provider 1 =========" + count);
        return "provider1";
    }
}
