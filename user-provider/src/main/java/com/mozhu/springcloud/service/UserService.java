package com.mozhu.springcloud.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserService {
    Logger logger = LoggerFactory.getLogger(UserService.class);

    @GetMapping("/login")
    public boolean login(String name, String password) {
        return "feign".equals(name) && "123456".equals(password);
    }

    @GetMapping("/testRibbon")
    public String testRibbon(int count) {
        logger.info("provider 0 =========" + count);
        return "provider0";
    }
}
