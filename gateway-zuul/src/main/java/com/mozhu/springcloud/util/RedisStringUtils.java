package com.mozhu.springcloud.util;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class RedisStringUtils {

    @Autowired
    private Gson gson;

    public static final int DEFAULT_TIME_OUT = 60 * 60 * 24;

    @Autowired
    private RedisTemplate redisTemplate;

    public String getKey(String key) {
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        String value = ops.get(key);
        return value;
    }

    public <T> T getKey(String key, Class<T> tClass) {
        String value = this.getKey(key);
        T t = gson.fromJson(value, tClass);
        return t;
    }

    public void setValue(String key, String value) {
        this.setValue(key, value, DEFAULT_TIME_OUT);
    }

    public void setValue(String key, String value, int exTime) {
        this.setValue(key,value,exTime,TimeUnit.SECONDS);
    }

    public void setValue(String key, String value, int exTime,TimeUnit timeUnit) {
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        ops.set(key, value, exTime, timeUnit);
    }

    public <T> void setValue(String key, T t) {
        this.setValue(key, t, DEFAULT_TIME_OUT);
    }

    public <T> void setValue(String key, T t, int exTime) {
        this.setValue(key, t, exTime,TimeUnit.SECONDS);
    }

    public <T> void setValue(String key, T t, int exTime,TimeUnit timeUnit) {
        String s = gson.toJson(t);
        this.setValue(key, s, exTime,timeUnit);
    }


}
