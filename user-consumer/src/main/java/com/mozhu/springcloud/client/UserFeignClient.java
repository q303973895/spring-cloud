package com.mozhu.springcloud.client;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "user-provider", fallback = UserFeignClientFail.class)
public interface UserFeignClient {

    @GetMapping("/login")
//    @LoadBalanced
    boolean login(@RequestParam String name, @RequestParam String password);

    @GetMapping("/testRibbon")
    String testRibbon(@RequestParam int count);
}
